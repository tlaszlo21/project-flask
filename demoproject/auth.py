from flask import Blueprint, render_template, request, flash, redirect, url_for
from .models import User
from werkzeug.security import generate_password_hash, check_password_hash
from . import db   ##means from __init__.py import db
from flask_login import login_user, login_required, logout_user, current_user



auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method =='POST':
        email = request.form.get('email')
        password = request.form.get('password')

        user = User.query.filter_by(email=email).first()
        if user:
            if check_password_hash(user.password, password):
                flash('Logged in successfully!', category='success')
                login_user(user, remember=True)
                return redirect(url_for('views.home'))
            else:
                flash('Incorrect password, try again!', category='error')
        #majd korlatozni kell a probalkozasok szamat
        else:
            flash('Email does not exist.', category='error')
            
    return render_template("login.html", user=current_user)


@auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('views.home'))



@auth.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    if request.method == "POST":
        email = request.form.get('email')
        first_name = request.form.get('firstName')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        user = User.query.filter_by(email=email).first()
    
        if len(email) < 4:
            flash('Email must be greater than 4 characters.', category='error')
        elif user:
            flash('Email is already registered. Please use a different email!', category='error')
        elif '@' not in email:
            flash('Email must contain "@" symbol.', category='error')
        elif len(first_name) < 2:
            flash('First name must be greater than 2 characters.', category='error')
        elif len(password1) < 8:
            flash('Password must be greater than 8 characters.', category='error')        
        elif password1 != password2:
            flash('Password confirmation doesn\'t match the original password. Please check and try again.', category='error')
        else:
            new_user = User(email=email, first_name=first_name, password=generate_password_hash(password1, method='pbkdf2:sha256'))
            db.session.add(new_user)
            db.session.commit()
            flash(f'Account created successfully!', category='success') 
            return redirect(url_for('views.home'))
#email confirmation
    return render_template("signup.html",user=current_user)