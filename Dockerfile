# Stage 1: Build stage
FROM python:3.8-slim-buster as builder

WORKDIR /app

# Copy requirements.txt to the container
COPY requirements.txt .
RUN pip install --upgrade pip

# Install dependencies
RUN pip install --no-cache-dir --upgrade -r requirements.txt

# Copy the application source code
COPY . .

# Stage 2: Production stage
FROM python:3.8-alpine

WORKDIR /app

# Copy only necessary files from the builder stage
COPY --from=builder /app /app
COPY --from=builder /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages
COPY --from=builder /usr/local/bin/gunicorn /usr/local/bin/gunicorn

# Expose port 5000
EXPOSE 5000

CMD ["gunicorn", "-b", "0.0.0.0:5000","-w","5", "main:app"]
